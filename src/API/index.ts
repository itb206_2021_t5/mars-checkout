import { TransactionSubmitData } from "@/typings";
import { Methods } from "@/typings/api";
import request from "./request";

const API = {
  fetchAllProducts() {
    return request(Methods.Get, `/product/`);
  },

  fetchAllPayments() {
    return request(Methods.Get, `/payment`);
  },

  // ASDAFG003

  // TODO: use use post
  fetchMemeberProfile(code: string) {
    return request(Methods.Get, `/member/${code}`);
  },

  createTransaction(data: TransactionSubmitData) {
    return request(Methods.Post, `/transaction`, data);
  },
};

export default API;
