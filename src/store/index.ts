import { createStore } from "vuex";

export default createStore({
  state: {
    isLoading: false,
    productList: null,
    paymentMethods: null,
    isAuthenticated: false,
  },
  mutations: {
    setIsLoading(state, bool: boolean) {
      state.isLoading = bool;
    },
    setProductList(state, products: any) {
      state.productList = products;
    },
    setPaymentMethods(state, payments: any) {
      state.paymentMethods = payments;
    },
    setAuthenticated(state, bool: boolean) {
      state.isAuthenticated = bool;
    },
  },
  actions: {
    setIsLoading(context, payload) {
      context.commit("setIsLoading", payload);
    },
    setProductList(context, payload) {
      context.commit("SetProductList", payload);
    },
    setPaymentMethods(context, payload) {
      context.commit("setPaymentMethods", payload);
    },
    setAuthenticated(context, payload) {
      context.commit("setProduct", payload);
    },
  },
  modules: {},
});
