import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faTimes,
  faArrowLeft,
  faArrowUp,
  faArrowDown,
  faBarcode,
  faTrash,
  faCheckCircle,
  faTimesCircle,
} from "@fortawesome/free-solid-svg-icons";
// import { faAlipay, faApplePay, faWeixin } from '@fortawesome/free-brands-svg-icons'

library.add(faTimes);
library.add(faArrowLeft);
library.add(faArrowUp);
library.add(faArrowDown);
library.add(faBarcode);
library.add(faTrash);
library.add(faCheckCircle);
library.add(faTimesCircle);
