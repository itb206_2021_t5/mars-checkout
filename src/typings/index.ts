export interface MemberProfile {
  mid: string
  name: string
  level: string
  points: number
  memberUntil: number
}

export interface CheckoutItem {
  barcode: string;
  brandName: string;
  productName: string;
  qty: number;
  originalPrice?: number;
  price: number;
}


export interface PaymentMethod {
  pyid: string
  code: string
  name: string
  type: string
}

export interface CartItem {
  pid: string
  qty: number
  price: number
}


// export interface Product {
//   pid: string
//   barcode: string
//   productName: string
//   brandName: string
//   size: string
//   price: number
// }


export interface Product {
  pid: string
  product_barcode: string
  product_name: string
  brand_name: string
  size_name: string
  price: number
}

export interface Payment {
  payment_id: string
  payment_code: string
  payment_name: string
  payment_type: string
}

export interface TransactionSubmitData {
  pyid: string
  mid: string
  products: {
    qty: number
    pid: string
    price: string
  }[]
}